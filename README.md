# Jurassic Clicking #

![size144.png](https://bitbucket.org/repo/5agndX/images/1726466268-size144.png)

A clicking game :
* Player is a hero that attacks the enemy (dinosaur) by clicking and gets coin after monster die.
* Player can buy the assistance and upgrade the his skill.

### Design Principle ###

* Game uses many principle such as Creator , Low Coupling, Controller.
* High Cohesions / SRP is used in each model class has a clear function such as assistant, hero, and monster.
* Polymorphism, Game extends observable of java for Observer Pattern.
* Indirection , Hero, Assistant and Monster are not know each other so they is call through Game.
* Open-closed principle , some part of Game can be touched by other class and some of it need other class to work on.

### Design Pattern ###

#### Observer ####
Game is observable. It notifies to GUI that is an observable such as when class monster in game die and drop gold. Game class notifies other class to know that it s need to update amount of gold that display in GUI.

#### Proxy ####
Proxy use in ListView to increase smoothness of whole application when activate.

#### Singleton ####
Each class is created only once. Class game is a singleton call every where that need game instance.

#### Adapter ####
Adapter work in get the data that get from class game and prepare it to show on list view.

#### MVC ####

Model : Assistant , Hero , Monster are class that operate something.
View : All UI , they is displayed for connect with player.
Controller : Game knows Model and UIs.Then,it will execute the game when player has any action.


### Member ###

* [Jidapar Jettanurak](https://bitbucket.org/5710546542)
* [Salilthip Phuklang](https://bitbucket.org/5710546640)
* [Woramate Jumroonsilp](https://bitbucket.org/5710547212)

ps. Thank you for every picture that we modify for this project.
ps2. Thank you for template from TheCheeseLibrary that we implement to our project.

###First Repository###
* [First Repository](https://bitbucket.org/b5710547212/softspecfinalproj)