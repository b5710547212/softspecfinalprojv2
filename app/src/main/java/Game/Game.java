package Game;

import com.example.woramatej.softspecfinalprojectv2.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import Model.Assistant;
import Model.Hero;
import Model.Monster;

/**
 * Game class that doing all function
 */
public class Game extends Observable{

    public final int CRITICAL = 0;
    private long criTimeStamp = 0;
    private boolean canCri = true;

    public final int GANK = 1;
    private long gankTimeStamp = 0;
    private boolean canGank = true;

    public final int HOTTIME = 2;
    private long hotTimeStamp = 0;
    private boolean canHot = true;
    private long hotStartTime = 0;
    private int hotTimeMul = 1;

    private Hero hero = null;
    private Monster monster = null;
    private List<Assistant>  assts = null;
    private int lvl = 0;
    private double gold = 0;


    private String[] asstsName = { "bulbasaur",
            "hitokage",
            "pikachu",
            "senikame",
            "pidgey",
            "magnimite",
            "growlithe",
            "gyarados",
            "magmar",
            "seadra",
            "butterfly",
            "hypn" };


    private int model = (int)Math.round( Math.random()*11 );

    public static Game instance = null;
    public static Game getInstance() {
        if ( instance == null ) instance = new Game();
        return instance;
    }

    private Game() {
        this.hero = Hero.getInstance();
        this.monster = Monster.getInstance();
        this.assts = new ArrayList<Assistant>();
    }
    public void loadGame( String data ) {
        Scanner scanner = new Scanner( data );
        this.setLvl( Integer.parseInt( scanner.next() ) );
        this.setGoold( Double.parseDouble( scanner.next() ) );
        int heroLvl = Integer.parseInt( scanner.next() );
        String heroName = scanner.next();
        this.hero.loadHero( heroLvl, heroName );
        if ( scanner.hasNext() ) this.setAssts( scanner.nextLine() );
        scanner.close();
    }
    public String getSaveData() {
        String temp = "";
        temp += this.lvl + " ";
        temp += this.gold + " ";
        temp += this.hero.getLvl() + " ";
        temp += this.hero.getName() + " ";
        for ( Assistant i : assts ) {
            temp += i.getLvl() + " ";
        }
        return temp;
    }
    public double getAsstsAtkDmg() {
        double tmp = 0;
        for ( Assistant i : assts ) tmp += i.getAtkDmg();
        return tmp;
    }
    public void asstsAtk() {
        Timer t1 = new Timer();
        t1.schedule(new TimerTask() {
            @Override
            public void run() {
                monster.recieveDmg(getAsstsAtkDmg());
            }
        }, 0, 1000);
    }

    public void tap() {
        this.monster.recieveDmg(this.hero.getAtkDmg());
    }

    public void start() {
        this.asstsAtk();
        Timer t2 = new Timer();
        t2.schedule(new TimerTask() {
            long curTime = System.currentTimeMillis();

            @Override
            public void run() {
                if (monster.isDeath()) {
                    model = (int) Math.round(Math.random() * 11);
                    gold += monster.getMaxHp() * hotTimeMul * 0.25;
                    upLvl();
                    monster.respawn();
                    setChanged();
                    notifyObservers();
                }
                if (curTime - hotStartTime >= 10000) {
                    hotTimeMul = 1;
//                    setChanged();
//                    notifyObservers();
                }
                if (curTime - criTimeStamp >= 180000) {
                    canCri = true;
//                    setChanged();
//                    notifyObservers();
                }
                if (curTime - gankTimeStamp >= 120000) {
                    canGank = true;
//                    setChanged();
//                    notifyObservers();
                }
                if (curTime - hotTimeStamp >= 300000) {
                    canHot = true;
//                    setChanged();
//                    notifyObservers();
                }
//                setChanged();
//                notifyObservers();
            }
        }, 0, 1);
    }

    public Hero getHero() { return this.hero; }
    public void setHero( Hero hero ) { this.hero = hero; }
    public void setHero( int lvl ) {}
    public void lvlUpHero() {
        if ( this.gold >= this.hero.getValue() ) {
            this.gold -= this.hero.getValue();
            this.hero.lvlUp();
            setChanged();
            notifyObservers();
        }
    }
    public String getHeroStat() { return String.format( "Level: \nTap DMG: %.0f", this.hero.getLvl(), this.hero.getAtkDmg() ); }

    public Monster getMonster() { return this.monster; }
    public void setMonster( Monster monster ) { this.monster = monster; }
    public void setMonster( int lvl ) {}

    public List<Assistant> getAssts() {
        if ( this.assts == null ) assts = new ArrayList<Assistant>();
        return this.assts;
    }
    public Assistant getAssistant( int index ) { return (Assistant)assts.get(index); }
    public void setAssts( List<Assistant> assts ) { this.assts = assts; }
    public void setAssts( String data ) {
        Scanner scanner = new Scanner( data );
        while ( scanner.hasNext() ) {
            this.assts.add( new Assistant( Integer.parseInt( scanner.next() ) ) );
        }
    }
    public void lvlUpAsst( int index ) {
        Assistant i = this.getAssistant( index );
        if ( this.gold >= i.getValue() ) {
            this.gold -= i.getValue();
            i.lvlUp();
            setChanged();
            notifyObservers();
        }
    }
    public void buyAsst() {
        if ( this.gold >= 10 && this.assts.size() < 12 ) {
            this.gold -= 10;
            this.assts.add(new Assistant());
        }
    }

    public double getGold() { return this.gold; }
    public void setGoold( double gold ) { this.gold = gold; }
    public String getGoldStr() { return String.format("%.0f", this.gold ); }

    public int getLvl() { return this.lvl; }
    public void setLvl( int lvl ) {
        this.lvl = lvl;
        this.monster.loadMonster( lvl );
    }
    public void upLvl() {
        this.lvl++;
        if ( this.lvl%10 == 0 ) this.monster.setMaxHp( this.monster.getNextLvlMaxHp() );
    }

    public void heroSkill( int skill ) {
        if ( skill == CRITICAL && this.canCri ) {
            this.monster.recieveDmg( this.hero.getAtkDmg()*this.hero.getCriticalHit() );
            this.canCri = false;
            this.criTimeStamp = System.currentTimeMillis();
        } else if ( skill == GANK && this.canGank ) {
            for ( int i = 0 ; i<this.hero.getGankHit() ; i++ ) this.monster.recieveDmg( this.getAsstsAtkDmg() );
            this.canGank = false;
            this.gankTimeStamp = System.currentTimeMillis();
        } else if ( skill == HOTTIME && this.canHot ) {
            this.hotTimeMul = this.hero.getHotTimeMul();
            this.canHot = false;
            this.hotTimeStamp = System.currentTimeMillis();
        }
    }
    public void upHeroSkill( int skill ) {
        if ( this.gold >= this.hero.getValue() ) {
            this.gold -= this.hero.getValue();
            if (skill == CRITICAL) {
                this.hero.upCriHit();
            } else if (skill == GANK) {
                this.hero.upGankHit();
            } else if (skill == HOTTIME) {
                this.hero.upHottime();
            }
        }
    }

    public String readValue( double input ) {
        return String.format( "%.0f", input );
    }
    public String getAsstStat( int index ) {
        Assistant i = this.assts.get( index );
        return String.format( "Level: %s\t%s\nDPS: %.0f\tPrice: %.0f", i.getLvl(), this.asstsName[ index ], i.getAtkDmg(), i.getValue() );
    }

    public boolean canUseSkill( int skill ) {
        if ( skill == CRITICAL ) return this.canCri;
        else if ( skill == GANK ) return this.canGank;
        else if ( skill == HOTTIME ) return this.canHot;
        return false;
    }
    public String getHeroSkillStat( int skill ) {
        if ( skill == this.CRITICAL )
            return String.format( "Level: %d\tCritical hit\nPrice: %.0f", this.hero.getCriHitLvl(), this.hero.getValue() );
        else if ( skill == this.GANK )
            return String.format( "Level: %d\tGank\nPrice: %.0f", this.hero.getGankLvl(), this.hero.getValue() );
        else if ( skill == this.HOTTIME )
            return String.format( "Level: %d\tHot Time\nPrice: %.0f", this.hero.getHotTimeLvl(), this.hero.getValue() );
        return null;
    }

    public int getModel() { return this.model; }

}
