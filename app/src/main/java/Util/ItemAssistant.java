package Util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.woramatej.softspecfinalprojectv2.R;

import Game.Game;

public class ItemAssistant extends BaseCustomViewGroup {

    private TextView description;
    private Button button;

    public ItemAssistant(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public ItemAssistant(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public ItemAssistant(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public ItemAssistant(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.list_item, this);
    }

    private void initInstances() {
        // findViewById here
        description = (TextView) findViewById(R.id.descriptionUpgradeAssis);
        button = (Button) findViewById(R.id.btnUpgradeAssis);
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        BundleSavedState savedState = new BundleSavedState(superState);
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);

        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        BundleSavedState ss = (BundleSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        Bundle bundle = ss.getBundle();
        // Restore State from bundle here
    }

    public void setItem(final int index){

//        Log.i("index ",index + "");
//                if(index == 0){
//                    image.setBackgroundResource(R.drawable.icon_bulbasaur);
//                }else if(index == 1){
//                    image.setBackgroundResource(R.drawable.icon_hitokage);
//                }else if(index == 2){
//                    image.setBackgroundResource(R.drawable.icon_pikachu);
//                }else if(index == 3){
//                    image.setBackgroundResource(R.drawable.icon_senikame);
//                }else if(index == 4){
//                    image.setBackgroundResource(R.drawable.icon_pidgey);
//                }else if(index == 5){
//                    image.setBackgroundResource(R.drawable.icon_magnimite);
//                }else if(index == 6){
//                    image.setBackgroundResource(R.drawable.icon_growlithe);
//                }else if(index == 7){
//                    image.setBackgroundResource(R.drawable.icon_gyarados);
//                }else if(index == 8){
//                    image.setBackgroundResource(R.drawable.icon_magmar);
//                }else if(index == 9){
//                    image.setBackgroundResource(R.drawable.icon_seadra);
//                }else if(index == 10){
//                    image.setBackgroundResource(R.drawable.icon_butterfly);
//                }else if(index == 11){
//                    image.setBackgroundResource(R.drawable.icon_hypno);
//                }

        description.post(new Runnable() {
            @Override
            public void run() {
                description.setText(Game.getInstance().getAsstStat(index));
            }
        });
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Game.getInstance().lvlUpAsst(index);
                    description.post(new Runnable() {
                        @Override
                        public void run() {
                            description.setText(Game.getInstance().getAsstStat(index));
                        }
                    });
                }
            });
       }

}
