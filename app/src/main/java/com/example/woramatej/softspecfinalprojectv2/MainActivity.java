package com.example.woramatej.softspecfinalprojectv2;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import Fragments.ButtonFragment;
import Fragments.MainFragment;
import Fragments.UpgradeHeroFragment;
import Game.Game;

public class MainActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Game.getInstance();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String data = settings.getString("data","1 0 1 Noot 0 0 0 0 0 0 0 0 0 0 0 0");
        Game.getInstance().loadGame(data);
        Game.getInstance().start();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, MainFragment.newInstance(), "MainFragment")
                    .add(R.id.contentContainerUpgrade, UpgradeHeroFragment.newInstance(), "UpgradeHeroFragment")
                    .add(R.id.contentContainerButton, ButtonFragment.newInstance(), "ButtonFragment")
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("data", Game.getInstance().getSaveData());
        editor.apply();
        editor.commit();
    }
}