package Model;

/**
 * Created by Woramate J on 9/5/2559.
 */
public class Hero {

    private String name = "";
    private final int maxLvl = 10;
    private int lvl = 0;
    private double atkDmg = 0;
    private double value = 0;

    private int criticalLvl = 0;
    private int criticalHit = 0;
    private int gankLvl = 0;
    private int gankHit = 0;
    private int hotTimeLvl = 0;
    private int hotTimeMul = 1;

    public static Hero instance = null;
    public static Hero getInstance() {
        if ( instance == null ) instance = new Hero();
        return instance;
    }

    private Hero() {
        this.lvl = 1;
        this.atkDmg = 1;
        this.value = 10;
    }

    public void loadHero( int lvl, String name ) {
        this.setLvl( lvl );
        this.setName( name );
    }

    public String getName() { return this.name; }
    public void setName( String name ) { this.name = name; }

    public int getMaxLvl() { return this.maxLvl; }
    public int getLvl() { return this.lvl; }
    public void setLvl( int lvl ) {
        if ( lvl < this.maxLvl ) this.lvl = lvl;
        this.setAtkDmg( lvl );
        this.setValue( lvl );
    }
    public void lvlUp() {
        if ( this.lvl+1 <= this.maxLvl ) this.lvl++;
        this.setLvl( lvl );
    }

    public double getAtkDmg() { return this.atkDmg; }
    public String getAtkDmgStr() { return String.format( "%.0f", this.atkDmg ); }
    public double getNextLvlAtkDmg() { return this.atkDmg*1.8; }
    private void setAtkDmg( int lvl ) {
        this.atkDmg = 1;
        for ( int i = 0 ; i<lvl ; i++ ) this.atkDmg = this.getNextLvlAtkDmg();
    }

    public double getValue() { return this.value; }
    public String getValueStr() { return String.format( "%.0f", this.value ); }
    public double getNextLvlValue() { return this.value*4; }
    private void setValue( int lvl ) {
        this.value = 10;
        for ( int i = 0 ; i<lvl ; i++ ) this.value = getNextLvlValue();
    }

    public int getCriHitLvl() { return this.criticalLvl; }
    public void upCriHit() {
        if ( this.criticalLvl == 0 ) {
            this.criticalLvl = 1;
            this.criticalHit = 2;
        }
        if ( this.criticalLvl < 10 ) {
            this.criticalLvl++;
            this.criticalHit*=this.criticalLvl;
        }
    }
    public int getCriticalHit() { return this.criticalHit; }

    public int getGankLvl() { return this.gankLvl; }
    public void upGankHit() {
        if ( this.gankLvl == 0 ) {
            this.gankLvl++;
            this.gankHit = 10;
        }
        if ( this.gankLvl < 10 ) {
            this.gankLvl++;
            this.gankHit+=this.gankLvl*10;
        }
    }
    public int getGankHit() { return this.gankHit; }

    public int getHotTimeLvl() { return this.hotTimeLvl; }
    public int getHotTimeMul() { return this.hotTimeMul; }
    public void upHottime() {
        if ( this.hotTimeLvl == 0 ) {
            this.hotTimeLvl++;
            this.hotTimeMul = 2;
        }
        if ( this.hotTimeLvl < 10 ) {
            this.hotTimeLvl++;
            this.hotTimeMul++;
        }
    }
}
