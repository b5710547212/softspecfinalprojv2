package Model;

/**
 * Created by Woramate J on 9/5/2559.
 */
public class Monster {

    private double maxHp = 0;
    private double hp = 0;

    public static Monster instance = null;
    public static Monster getInstance() {
        if ( instance == null ) instance = new Monster();
        return instance;
    }

    private Monster() {
        this.maxHp = 10;
        this.hp = this.maxHp;
    }

    public void loadMonster( int gameLvl ) {
        for ( int i = 0 ; i<gameLvl ; i++ ) this.setMaxHp( this.getNextLvlMaxHp() );
    }

    public void recieveDmg( double dmg ) { this.hp -= dmg; }
    public void respawn() { this.hp = this.maxHp; }
    public boolean isDeath() { return this.hp <= 0; }

    public double getMaxHp() { return this.maxHp; }
    public double getNextLvlMaxHp() { return this.maxHp*4; }
    public void setMaxHp( double maxHp ) { this.maxHp = maxHp; }

    public double getHp() { return this.hp; }
    public void setHp( double hp ) { this.hp = hp; }

}
