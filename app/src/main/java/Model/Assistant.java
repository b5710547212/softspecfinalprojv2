package Model;

public class Assistant {

    private final int maxLvl = 100;
    private int lvl = 0;
    private double atkDmg = 0;
    private double value = 0;

    public Assistant() {
        this.value = 10;
    }

    public Assistant( int lvl ) {
        this.setLvl( lvl );
    }

    public int getMaxLvl() { return this.maxLvl; }

    public int getLvl() { return this.lvl; }
    public void setLvl( int lvl ) {
        this.lvl = lvl;
        this.setAtkDmg( lvl );
        this.setValue( lvl );
    }
    public void lvlUp() {
        if ( this.lvl+1 <= this.maxLvl ) this.lvl++;
        this.setLvl( lvl );
    }

    public double getAtkDmg() { return this.atkDmg; }
    public double getNextLvlAtkDmg() { return this.atkDmg*1.8; }
    public void setAtkDmg( int lvl ) {
        if ( lvl == 0 ) {
            this.atkDmg = 0;
            return;
        }
        this.atkDmg = 1;
        for ( int i = 0 ; i<lvl ; i++ ) this.atkDmg = this.getNextLvlAtkDmg();
    }

    public double getValue() { return this.value; }
    public double getNextLvlValue() { return this.value*4; }
    private void setValue( int lvl ) {
        this.value = 10;
        for ( int i = 0 ; i<lvl ; i++ ) this.value = getNextLvlValue();
    }

}
