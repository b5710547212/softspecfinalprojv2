package Adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import Game.Game;
import Model.Assistant;
import Util.ItemAssistant;

/**
 * Created by Noot on 29/5/2559.
 */
public class AssistantAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return Game.getInstance().getAssts().size();
    }

    @Override
    public Assistant getItem(int position) {
        return Game.getInstance().getAssts().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemAssistant item;
        if(convertView!=null){
            item = (ItemAssistant)convertView;
        }else {
            item = new ItemAssistant(parent.getContext());
        }

        item.setItem(position);
        return item;
    }
}
