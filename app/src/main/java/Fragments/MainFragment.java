package Fragments;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.woramatej.softspecfinalprojectv2.R;

import java.util.Observable;
import java.util.Observer;

import Game.Game;


public class MainFragment extends Fragment implements Observer{

    private AnimationDrawable frameAnimation;
    private Thread progressBarThread;
    private RelativeLayout background;
    private ProgressBar monsterHp;
    private EditText imageMoney;
    private EditText imageLevel;
    private ImageView imageMonster;
    private ImageView imageAssistant1;
    private ImageView imageAssistant2;
    private ImageView imageAssistant3;
    private ImageView imageAssistant4;
    private ImageView imageAssistant5;
    private ImageView imageAssistant6;
    private ImageView imageAssistant7;
    private ImageView imageAssistant8;
    private ImageView imageAssistant9;
    private ImageView imageAssistant10;
    private ImageView imageAssistant11;
    private ImageView imageAssistant12;
    private AnimationDrawable frameAnimationAssis1;
    private AnimationDrawable frameAnimationAssis2;
    private AnimationDrawable frameAnimationAssis3;
    private AnimationDrawable frameAnimationAssis4;
    private AnimationDrawable frameAnimationAssis5;
    private AnimationDrawable frameAnimationAssis6;
    private AnimationDrawable frameAnimationAssis7;
    private AnimationDrawable frameAnimationAssis8;
    private AnimationDrawable frameAnimationAssis9;
    private AnimationDrawable frameAnimationAssis10;
    private AnimationDrawable frameAnimationAssis11;
    private AnimationDrawable frameAnimationAssis12;

    public MainFragment() {
        super();
        Game.getInstance().addObserver(this);
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        imageMonster = (ImageView)rootView.findViewById(R.id.imageMonster);


        imageLevel = (EditText)rootView.findViewById(R.id.imageLevel);
        imageLevel.setText(Game.getInstance().getLvl() + "");

        imageMoney = (EditText) rootView.findViewById(R.id.imageMoney);
        imageMoney.setText(Game.getInstance().getGoldStr());

        background = (RelativeLayout) rootView.findViewById(R.id.background);
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.getInstance().tap();
            }
        });


        monsterHp = (ProgressBar) rootView.findViewById(R.id.progressHpMonster);
        progressBarThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    monsterHp.setProgress((int) ((Game.getInstance().getMonster().getHp() / Game.getInstance().getMonster().getMaxHp()) * 100));

                }
            }
        });
        progressBarThread.start();

        //assistant
        imageAssistant1 = (ImageView) rootView.findViewById(R.id.imageAssistant1);
        imageAssistant2 = (ImageView) rootView.findViewById(R.id.imageAssistant2);
        imageAssistant3 = (ImageView) rootView.findViewById(R.id.imageAssistant3);
        imageAssistant4 = (ImageView) rootView.findViewById(R.id.imageAssistant4);
        imageAssistant5 = (ImageView) rootView.findViewById(R.id.imageAssistant5);
        imageAssistant6 = (ImageView) rootView.findViewById(R.id.imageAssistant6);
        imageAssistant7 = (ImageView) rootView.findViewById(R.id.imageAssistant7);
        imageAssistant8 = (ImageView) rootView.findViewById(R.id.imageAssistant8);
        imageAssistant9 = (ImageView) rootView.findViewById(R.id.imageAssistant9);
        imageAssistant10 = (ImageView) rootView.findViewById(R.id.imageAssistant10);
        imageAssistant11 = (ImageView) rootView.findViewById(R.id.imageAssistant11);
        imageAssistant12 = (ImageView) rootView.findViewById(R.id.imageAssistant12);

        imageAssistant1.setBackgroundResource(R.drawable.bulbasaur_animation);
        frameAnimationAssis1 = (AnimationDrawable) imageAssistant1.getBackground();
        frameAnimationAssis1.start();

        imageAssistant2.setBackgroundResource(R.drawable.butterfly_animation);
        frameAnimationAssis2 = (AnimationDrawable) imageAssistant2.getBackground();
        frameAnimationAssis2.start();

        imageAssistant3.setBackgroundResource(R.drawable.growlithe_animation);
        frameAnimationAssis3 = (AnimationDrawable) imageAssistant3.getBackground();
        frameAnimationAssis3.start();

        imageAssistant4.setBackgroundResource(R.drawable.gyarados_animation);
        frameAnimationAssis4 = (AnimationDrawable) imageAssistant4.getBackground();
        frameAnimationAssis4.start();

        imageAssistant5.setBackgroundResource(R.drawable.hitokage_animation);
        frameAnimationAssis5 = (AnimationDrawable) imageAssistant5.getBackground();
        frameAnimationAssis5.start();

        imageAssistant6.setBackgroundResource(R.drawable.hypno_animation);
        frameAnimationAssis6 = (AnimationDrawable) imageAssistant6.getBackground();
        frameAnimationAssis6.start();

        imageAssistant7.setBackgroundResource(R.drawable.magmar_animation);
        frameAnimationAssis7 = (AnimationDrawable) imageAssistant7.getBackground();
        frameAnimationAssis7.start();

        imageAssistant8.setBackgroundResource(R.drawable.magnimite_animation);
        frameAnimationAssis8 = (AnimationDrawable) imageAssistant8.getBackground();
        frameAnimationAssis8.start();

        imageAssistant9.setBackgroundResource(R.drawable.pidgey_animation);
        frameAnimationAssis9 = (AnimationDrawable) imageAssistant9.getBackground();
        frameAnimationAssis9.start();

        imageAssistant10.setBackgroundResource(R.drawable.pikachu_animation);
        frameAnimationAssis10 = (AnimationDrawable) imageAssistant10.getBackground();
        frameAnimationAssis10.start();

        imageAssistant11.setBackgroundResource(R.drawable.seadra_animation);
        frameAnimationAssis11 = (AnimationDrawable) imageAssistant11.getBackground();
        frameAnimationAssis11.start();

        imageAssistant12.setBackgroundResource(R.drawable.senikame_animation);
        frameAnimationAssis12 = (AnimationDrawable) imageAssistant12.getBackground();
        frameAnimationAssis12.start();

        onResume();

    }

    @Override
    public void onResume() {
        super.onResume();

//        Log.w("hp ", (int) (Game.getInstance().getMonster().getHp() / Game.getInstance().getMonster().getMaxHp()) * 100 + "");
        View decorView = getActivity().getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        final int model = Game.getInstance().getModel();
        imageMonster.post(new Runnable() {
            @Override
            public void run() {
                if (model == 0) imageMonster.setBackgroundResource(R.drawable.bird_animation);
                else if (model == 1)
                    imageMonster.setBackgroundResource(R.drawable.blue_silly_animation);
                else if (model == 2) imageMonster.setBackgroundResource(R.drawable.flyer_animation);
                else if (model == 3)
                    imageMonster.setBackgroundResource(R.drawable.lambeosaurus_animation);
                else if (model == 4)
                    imageMonster.setBackgroundResource(R.drawable.pliosaur_animation);
                else if (model == 5)
                    imageMonster.setBackgroundResource(R.drawable.raptor_animation);
                else if (model == 6)
                    imageMonster.setBackgroundResource(R.drawable.salamander_animation);
                else if (model == 7)
                    imageMonster.setBackgroundResource(R.drawable.sharpteeth_animation);
                else if (model == 8) imageMonster.setBackgroundResource(R.drawable.silly_animation);
                else if (model == 9) imageMonster.setBackgroundResource(R.drawable.trex_animation);
                frameAnimation = (AnimationDrawable) imageMonster.getBackground();
                frameAnimation.start();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        progressBarThread.interrupt();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    @Override
    public void update(Observable observable, Object data) {
        imageMoney.post(new Runnable() {
            @Override
            public void run() {
                imageMoney.setText(Game.getInstance().getGoldStr());
            }
        });

        imageLevel.post(new Runnable() {
            @Override
            public void run() {
                imageLevel.setText(Game.getInstance().getLvl() + "");
            }
        });
        onResume();

    }
}
