package Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;


import com.example.woramatej.softspecfinalprojectv2.R;

import Adapter.AssistantAdapter;

public class UpgradeAssistantFragment extends Fragment {

    private ListView assistantList;
    private AssistantAdapter assistantAdapter;

    public UpgradeAssistantFragment() {
        super();
    }

    public static UpgradeAssistantFragment newInstance() {
        UpgradeAssistantFragment fragment = new UpgradeAssistantFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_upgrade_assistant, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        assistantList = (ListView)rootView.findViewById(R.id.assistantList);
        assistantAdapter = new AssistantAdapter();
        assistantList.setAdapter(assistantAdapter);

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
