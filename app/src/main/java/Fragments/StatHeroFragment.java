package Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.woramatej.softspecfinalprojectv2.R;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import Game.Game;


public class StatHeroFragment extends Fragment /*implements Observer */ {

    private Button skill1;
    private Button skill2;
    private Button skill3;
    private EditText dps;
    private EditText dpt;


    public StatHeroFragment() {
        super();
//        Game.getInstance().addObserver(this);
    }

    public static StatHeroFragment newInstance() {
        StatHeroFragment fragment = new StatHeroFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stat_hero, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        dps = (EditText) rootView.findViewById(R.id.image_dps);
        dpt = (EditText) rootView.findViewById(R.id.image_dpt);

        skill1 = (Button) rootView.findViewById(R.id.skill1);
        skill1.setOnClickListener(listener);
        skill2 = (Button) rootView.findViewById(R.id.skill2);
        skill2.setOnClickListener(listener);
        skill3 = (Button) rootView.findViewById(R.id.skill3);
        skill3.setOnClickListener(listener);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                dps.post(new Runnable() {
                    @Override
                    public void run() {
                        dps.setText(Game.getInstance().getAsstsAtkDmg() + "");
                    }
                });
                dpt.post(new Runnable() {
                    @Override
                    public void run() {
                        dpt.setText(Game.getInstance().getHero().getAtkDmgStr());
                    }
                });
            }
        }, 0, 1000);

    }


View.OnClickListener listener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (v == skill1) {
            Game.getInstance().heroSkill(Game.getInstance().CRITICAL);
            skill1.setBackgroundResource(R.drawable.cooldown1);
            skill1.setEnabled(false);

            Toast.makeText(getContext(),"Skill1",Toast.LENGTH_SHORT).show();
        } else if (v == skill2) {
            Game.getInstance().heroSkill(Game.getInstance().GANK);
            Toast.makeText(getContext(), "Skill2", Toast.LENGTH_SHORT).show();
        } else if (v == skill3) {
            Game.getInstance().heroSkill(Game.getInstance().HOTTIME);
            Toast.makeText(getContext(), "Skill3", Toast.LENGTH_SHORT).show();
        }
    }
};

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

//    @Override
//    public void update(Observable observable, Object data) {
//        if (Game.getInstance().canUseSkill(Game.getInstance().CRITICAL)) {
//            skill1.setBackgroundResource(R.drawable.skill1);
//        } else {
//            skill1.setBackgroundResource(R.drawable.cooldown1);
//        }
//
//        if (Game.getInstance().canUseSkill(Game.getInstance().GANK)) {
//            skill1.setBackgroundResource(R.drawable.skill2);
//        } else {
//            skill1.setBackgroundResource(R.drawable.cooldown2);
//        }
//
//        if (Game.getInstance().canUseSkill(Game.getInstance().HOTTIME)) {
//            skill1.setBackgroundResource(R.drawable.skill3);
//        } else{
//            skill1.setBackgroundResource(R.drawable.cooldown3);
//
//        }
//
//    }
//    @Override
//    public void update(Observable observable, Object data) {
//        if (Game.getInstance().canUseSkill(Game.getInstance().CRITICAL)) {
//            skill1.setBackgroundResource(R.drawable.skill1);
//        } else {
//            skill1.setBackgroundResource(R.drawable.cooldown1);
//        }
//        if (Game.getInstance().canUseSkill(Game.getInstance().GANK)) {
//            skill1.setBackgroundResource(R.drawable.skill2);
//        } else {
//            skill1.setBackgroundResource(R.drawable.cooldown2);
//        }
//        if (Game.getInstance().canUseSkill(Game.getInstance().HOTTIME)) {
//            skill1.setBackgroundResource(R.drawable.skill3);
//        } else{
//            skill1.setBackgroundResource(R.drawable.cooldown3);
//        }
//    }
}
