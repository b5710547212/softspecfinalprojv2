package Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.woramatej.softspecfinalprojectv2.R;

import Game.Game;

public class UpgradeHeroFragment extends Fragment {


    private ImageView imageUpgradehero;
    private TextView descriptionUpgradeHero;
    private TextView descriptionUpgradeSkill1;
    private TextView descriptionUpgradeSkill2;
    private TextView descriptionUpgradeSkill3;
    private Button buttonUpgradeHero;
    private Button btnUpgradeSkill1;
    private Button btnUpgradeSkill2;
    private Button btnUpgradeSkill3;

    public UpgradeHeroFragment() {
        super();
    }

    public static UpgradeHeroFragment newInstance() {
        UpgradeHeroFragment fragment = new UpgradeHeroFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_upgrade_hero, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here

        imageUpgradehero = (ImageView) rootView.findViewById(R.id.imageUpgradeHero);
        descriptionUpgradeHero = (TextView) rootView.findViewById(R.id.descriptionUpgradeHero);
        descriptionUpgradeHero.setText("Hero Lv." + Game.getInstance().getHero().getLvl() + "\tPrice:" + Game.getInstance().getHero().getValueStr() + "\nAttack : " + Game.getInstance().getHero().getAtkDmgStr());

        buttonUpgradeHero = (Button) rootView.findViewById(R.id.btnUpgradeHero);
        buttonUpgradeHero.setOnClickListener(listener);

        btnUpgradeSkill1 = (Button) rootView.findViewById(R.id.btnUpgradeSkill1);
        btnUpgradeSkill1.setOnClickListener(listener);
        btnUpgradeSkill2 = (Button) rootView.findViewById(R.id.btnUpgradeSkill2);
        btnUpgradeSkill2.setOnClickListener(listener);
        btnUpgradeSkill3 = (Button) rootView.findViewById(R.id.btnUpgradeSkill3);
        btnUpgradeSkill3.setOnClickListener(listener);

        descriptionUpgradeSkill1 = (TextView) rootView.findViewById(R.id.descriptionUpgradeSkill1);
        descriptionUpgradeSkill1.setText(Game.getInstance().getHeroSkillStat(Game.getInstance().CRITICAL));
        descriptionUpgradeSkill2 = (TextView) rootView.findViewById(R.id.descriptionUpgradeSkill2);
        descriptionUpgradeSkill2.setText(Game.getInstance().getHeroSkillStat(Game.getInstance().GANK));
        descriptionUpgradeSkill3 = (TextView) rootView.findViewById(R.id.descriptionUpgradeSkill3);
        descriptionUpgradeSkill3.setText(Game.getInstance().getHeroSkillStat(Game.getInstance().HOTTIME));

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == buttonUpgradeHero){
                Game.getInstance().lvlUpHero();
                descriptionUpgradeHero.setText("Hero Lv." + Game.getInstance().getHero().getLvl() + "\tPrice:" + Game.getInstance().getHero().getValueStr() + "\nAttack : " + Game.getInstance().getHero().getAtkDmgStr());
            }else if (v == btnUpgradeSkill1){
                Game.getInstance().upHeroSkill(Game.getInstance().CRITICAL);
            }else if (v == btnUpgradeSkill2){
                Game.getInstance().upHeroSkill(Game.getInstance().GANK);
            }else if (v == btnUpgradeSkill3){
                Game.getInstance().upHeroSkill(Game.getInstance().HOTTIME);

            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
