package Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.woramatej.softspecfinalprojectv2.R;



public class ButtonFragment extends Fragment {

    private Button btn1;
    private Button btn2;
    private Button btn3;

    public ButtonFragment() {
        super();
    }

    public static ButtonFragment newInstance() {
        ButtonFragment fragment = new ButtonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_button, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        btn1 = (Button)rootView.findViewById(R.id.btn1);
        btn1.setOnClickListener(listener);
        btn2 = (Button)rootView.findViewById(R.id.btn2);
        btn2.setOnClickListener(listener);
        btn3 = (Button)rootView.findViewById(R.id.btn3);
        btn3.setOnClickListener(listener);

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == btn1){
                btn1.setBackgroundResource(R.drawable.assist);
                btn2.setBackgroundResource(R.drawable.fade_hero);
                btn3.setBackgroundResource(R.drawable.fade_attack);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainerUpgrade, UpgradeAssistantFragment.newInstance(), "UpgradeAssistantFragment").commit();
            }else if(v == btn2){
                btn1.setBackgroundResource(R.drawable.fade_assit);
                btn2.setBackgroundResource(R.drawable.hero);
                btn3.setBackgroundResource(R.drawable.fade_attack);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainerUpgrade,UpgradeHeroFragment.newInstance(),"UpgradeHeroFragment").commit();
            }else if(v == btn3){
                btn1.setBackgroundResource(R.drawable.fade_assit);
                btn2.setBackgroundResource(R.drawable.fade_hero);
                btn3.setBackgroundResource(R.drawable.attack);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainerUpgrade, StatHeroFragment.newInstance(), "StatHeroFragment").commit();
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
